opam-version: "2.0"
synopsis:
  "Official release 4.07.1, with frame-pointers and flambda activated"
maintainer: "platform@lists.ocaml.org"
depends: [
  "ocaml" {= "4.07.1" & post}
  "base-unix" {post}
  "base-bigarray" {post}
  "base-threads" {post}
]
conflict-class: "ocaml-core-compiler"
flags: compiler
setenv: CAML_LD_LIBRARY_PATH = "%{lib}%/stublibs"
build: [
  [
    "./configure"
    "-prefix"
    prefix
    "-with-debug-runtime"
    "-with-frame-pointers"
    "-flambda"
  ] {os != "openbsd" & os != "freebsd" & os != "macos"}
  [
    "./configure"
    "-prefix"
    prefix
    "-with-debug-runtime"
    "-with-frame-pointers"
    "-flambda"
    "-cc"
    "cc"
    "-aspp"
    "cc -c"
  ] {os = "openbsd" | os = "freebsd" | os = "macos"}
  [make "-j%{jobs}%" {os != "cygwin"} "world"]
  [make "-j%{jobs}%" {os != "cygwin"} "world.opt"]
]
install: [make "install"]
url {
  src: "https://github.com/ocaml/ocaml/archive/4.07.1.tar.gz"
  checksum: [
    "md5=352fe8d46cb238a26aa10c38bad6ecb6"
    "sha256=83683ddad54bd23773591a9f757e702fa5cfa2ea1b124d8fe75a73729e592bfe"
    "sha512=46d9c637cb4e0dfc3199fba3668973d6668c7e2160bc2b689e0b2e762a665edc4327866ec90b9d17c336d96bf3a8ceafe923238dd9b4fe15cef00222c790fa38"
  ]
}
post-messages: [
  "A failure in the middle of the build may be caused by build parallelism
   (enabled by default).
   Please file a bug report at https://github.com/ocaml/ocaml/issues"
  {failure & jobs > 1 & os != "cygwin"}
  "You can try installing again including --jobs=1
   to force a sequential build instead."
  {failure & jobs > 1 & os != "cygwin" & opam-version >= "2.0.5"}
]
