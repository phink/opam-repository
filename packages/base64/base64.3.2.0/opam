opam-version: "2.0"
maintainer: "mirageos-devel@lists.xenproject.org"
authors: [ "Thomas Gazagnaire"
           "Anil Madhavapeddy" "Calascibetta Romain"
           "Peter Zotov" ]
license: "ISC"
homepage: "https://github.com/mirage/ocaml-base64"
doc: "http://mirage.github.io/ocaml-base64/"
bug-reports: "https://github.com/mirage/ocaml-base64/issues"
dev-repo: "git+https://github.com/mirage/ocaml-base64.git"
synopsis: "Base64 encoding for OCaml"
description: """
Base64 is a group of similar binary-to-text encoding schemes that represent
binary data in an ASCII string format by translating it into a radix-64
representation.  It is specified in RFC 4648.
"""
depends: [
  "ocaml" {>="4.03.0"}
  "base-bytes"
  "dune" {>= "1.0.1"}
  "bos" {with-test}
  "rresult" {with-test}
  "alcotest" {with-test}
]
build: [
  ["dune" "subst"]
  ["dune" "build" "-p" name "-j" jobs]
  ["dune" "runtest" "-p" name] {with-test}
]
url {
  src:
    "https://github.com/mirage/ocaml-base64/releases/download/v3.2.0/base64-v3.2.0.tbz"
  checksum: [
    "md5=8ac1d6145277cee57d36611d1c420f05"
    "sha256=b6717d5540d22a51e0ce473f94e89acbf6592f234c3d096449e7ed4796854ecb"
    "sha512=8411b1a8881d37a1ef127ffad0f82d15a5aa9c9889df4c92adda6f1e4f767afb2f262eec5cbef936166f5b07dc796b6c99becd02f5456841695264f3e906d8f3"
  ]
}
